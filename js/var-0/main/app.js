function bowdlerize(input, dictionary) {
  function invalidFormat(word) {
    return typeof word !== "string";
  }
  function hiddenChars(length) {
    var template = "";

    while (length) {
      template += "*";
      length--;
    }
    return template;
  }
  if (typeof input !== "string") {
    throw "Input should be a string";
  }
  if (
    !Array.isArray(dictionary) ||
    !dictionary.length ||
    dictionary.some(invalidFormat)
  ) {
    throw "Invalid dictionary format";
  }
  dictionary.forEach(word => {
    var indexOfWord = input.toLowerCase().indexOf(word);

    while (indexOfWord !== -1) {
      input =
        input.substring(0, indexOfWord + 1) +
        hiddenChars(word.length - 2) +
        input.substring(indexOfWord + word.length - 1);
      indexOfWord = input.toLowerCase().indexOf(word);
    }
  });
  return input;
}

const app = {
  bowdlerize
};

module.exports = app;
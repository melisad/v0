const express = require('express')

const app = express()

app.get('/cars', (req, res) => {
    res.status(200).json([{
        name :  'a',
        color : 'red'
    },{
        name :  'b',
        color : 'blue'
    }])
})
app.use(express.static("./public")).get("*", (req, res) => {
  res.status(200).sendFile("./index.html");
});

module.exports = app